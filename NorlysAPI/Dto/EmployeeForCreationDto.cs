﻿namespace NorlysAPI.Dto
{
    public class EmployeeForCreationDto
    {  
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDate { get; set; }
        public int OfficeID { get; set; }
    }
}
