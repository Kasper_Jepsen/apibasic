﻿using NorlysAPI.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using NorlysAPI.Dto;
using NorlysAPI.Services;

namespace NorlysAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {

        private readonly IEmployeeRepository _employeeRepo;
        private readonly IEmployeeValidationService _employeeValidationService;

        public EmployeeController(IEmployeeRepository employeeRepo, IEmployeeValidationService employeeValidationService)
        {
            _employeeRepo = employeeRepo;
            _employeeValidationService = employeeValidationService;
        }

        [HttpGet]
        public async Task<IActionResult> GetEmployees()
        {
            try
            {
                var employees = await _employeeRepo.GetEmployees();
                return Ok(employees);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}", Name = "EmployeeByID")]
        public async Task<IActionResult> GetEmployee(int id)
        {
            try
            {
                var employee = await _employeeRepo.GetEmployee(id);
                if (employee == null)
                {
                    return NotFound();
                }
                return Ok(employee);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }


        [HttpPost]
        public async Task<IActionResult> CreateEmployee(EmployeeForCreationDto employee)
        {
            if (!_employeeValidationService.ValidateEmployeeAge(employee.BirthDate))
            {
                return StatusCode(400, "Employee invalid Age");
            }
            if (!_employeeValidationService.ValidateEmployeeLastName(employee.LastName))
            {
                return StatusCode(400, "Employee LastName contains WhiteSpace");
            }
            if (!await _employeeValidationService.ValidateEmployeeOfficeSpace(employee.OfficeID))
            {
                return StatusCode(400, "Office is at capacity");
            }
            try
            {
                var createdEmployee = await _employeeRepo.CreateEmployee(employee);
                return CreatedAtRoute("EmployeeByID", new { id = createdEmployee.EmployeeID }, createdEmployee);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEmployee(int id, EmployeeForUpdateDto employee)
        {
            if (!_employeeValidationService.ValidateEmployeeAge(employee.BirthDate))
            {
                return StatusCode(400, "Employee invalid Age");
            }
            if (!_employeeValidationService.ValidateEmployeeLastName(employee.LastName))
            {
                return StatusCode(400, "Employee LastName contains WhiteSpace");
            }
            if (!await _employeeValidationService.ValidateEmployeeOfficeSpace(employee.OfficeID))
            {
                return StatusCode(400, "Office is at capacity");
            }
            try
            {
                var dbEmployee = await _employeeRepo.GetEmployee(id);
                if (dbEmployee == null)
                    return NotFound();

                await _employeeRepo.UpdateEmployee(id, employee);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(int id)
        {
            try
            {
                var dbEmployee = await _employeeRepo.GetEmployee(id);
                if (dbEmployee == null)
                    return NotFound();

                await _employeeRepo.DeleteEmployee(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
    }
}