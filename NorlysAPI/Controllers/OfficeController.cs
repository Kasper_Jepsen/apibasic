﻿using NorlysAPI.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;




namespace NorlysAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OfficeController : ControllerBase
    {
        private readonly IOfficeRepository _officeRepo;

        public OfficeController(IOfficeRepository officeRepo)
        {
            _officeRepo = officeRepo;
        }


        [HttpGet]
        public async Task<IActionResult> GetOffices()
        {
            try
            {
                var offices = await _officeRepo.GetOffices();
                return Ok(offices);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOffice(int id)
        {
            try
            {
                var office = await _officeRepo.GetOffice(id);
                return Ok(office);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
