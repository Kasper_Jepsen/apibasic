﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NorlysAPI.Models;

namespace NorlysAPI.Contracts
{
    public interface IOfficeRepository
    {
        public Task<IEnumerable<Office>> GetOffices();
        public Task<Office> GetOffice(int id);
    }
}
