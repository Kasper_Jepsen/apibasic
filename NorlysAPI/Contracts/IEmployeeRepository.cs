﻿using NorlysAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using NorlysAPI.Dto;

namespace NorlysAPI.Contracts
{
    public interface IEmployeeRepository
    {
        public Task<IEnumerable<Employee>> GetEmployees();
        public Task<Employee> GetEmployee(int id);

        public Task<List<Employee>> GetEmployeesInOffice(int officeID);

        public Task<Employee> CreateEmployee(EmployeeForCreationDto employee);

        public Task UpdateEmployee(int id, EmployeeForUpdateDto employee);

        public Task DeleteEmployee(int id);
        
    }
}
