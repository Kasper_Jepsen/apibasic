﻿using NorlysAPI.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorlysAPI.Services
{
    public class EmployeeValidationService : IEmployeeValidationService
    {
        private readonly IEmployeeRepository _employeeRepo;

        public EmployeeValidationService(IEmployeeRepository employeeRepo)
        {
            _employeeRepo = employeeRepo;
        }
        public bool ValidateEmployeeAge(string birthDate)
        {
            return ((DateTime.Today.Subtract(DateTime.Parse(birthDate)).Days / 365) < 100);
        }

        public bool ValidateEmployeeLastName(string lastName)
        {
            return !lastName.Contains(" ");
        }

        public async Task<bool> ValidateEmployeeOfficeSpace(int officeID)
        {
            return ((await _employeeRepo.GetEmployeesInOffice(officeID)).Count <= 5);
        }
    }
}
