﻿using NorlysAPI.Dto;
using NorlysAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorlysAPI.Services
{
    public interface IEmployeeValidationService
    {
        bool ValidateEmployeeAge(string birthDate);

        bool ValidateEmployeeLastName(string lastName);

        Task<bool> ValidateEmployeeOfficeSpace(int officeID);
    }
}
