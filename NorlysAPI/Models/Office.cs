﻿namespace NorlysAPI.Models
{
    public class Office
    {
        public int OfficeID { get; set; }
        public string OfficeName { get; set; }

    }
}
