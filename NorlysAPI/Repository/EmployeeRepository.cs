﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NorlysAPI.Contracts;
using NorlysAPI.Context;
using NorlysAPI.Models;
using NorlysAPI.Dto;
using Dapper;

namespace NorlysAPI.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly DapperContext _context;

        public EmployeeRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Employee>> GetEmployees()
        {
            var query = @"select EmployeeId, 
                        FirstName, LastName, BirthDate, OfficeID 
                        from dbo.Employee";

            using (var connection = _context.CreateConnection())
            {
                var employees = await connection.QueryAsync<Employee>(query);
                return employees.ToList();
            }
        }
        public async Task<Employee> GetEmployee(int id)
        {
            var query = "Select * from dbo.Employee where EmployeeID = @id";

            using (var connection = _context.CreateConnection())
            {
                var employee = await connection.QuerySingleOrDefaultAsync<Employee>(query, new { id });

                return employee;
            }
        }
        public async Task<List<Employee>> GetEmployeesInOffice(int officeID)
        {
            var query = "Select * from dbo.Employee where OfficeID = @officeID";

            using (var connection = _context.CreateConnection())
            {
                var employees = await connection.QueryAsync<Employee>(query, new { officeID });

                return employees.ToList();
            }
        }



        public async Task<Employee> CreateEmployee(EmployeeForCreationDto employee)
        {
            var query = @"
                           insert into dbo.Employee (FirstName, LastName, BirthDate, OfficeID)
                           values (@FirstName, @LastName, @BirthDate, @OfficeID)
                           Select Cast(SCOPE_IDENTITY() as int)
                            ";
            var parameters = new DynamicParameters();
            parameters.Add("FirstName", employee.FirstName);
            parameters.Add("LastName", employee.LastName);
            parameters.Add("BirthDate", employee.BirthDate);
            parameters.Add("OfficeID", employee.OfficeID);

            using (var connection = _context.CreateConnection())
            {
                var id = await connection.QuerySingleAsync<int>(query, parameters);
                var createdEmployee = new Employee
                {
                    EmployeeID = id,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    BirthDate = employee.BirthDate,
                    OfficeID = employee.OfficeID
                };

                return createdEmployee;
            }
        }
        public async Task UpdateEmployee(int id, EmployeeForUpdateDto employee)
        {
            var query = @"
                           update dbo.Employee
                           set FirstName = @FirstName,
                           LastName = @LastName,
                           BirthDate = @BirthDate,
                           OfficeID = @OfficeID
                           where EmployeeID = @EmployeeID
                           ";

            var parameters = new DynamicParameters();
            parameters.Add("EmployeeID", id);
            parameters.Add("FirstName", employee.FirstName);
            parameters.Add("LastName", employee.LastName);
            parameters.Add("BirthDate", employee.BirthDate);
            parameters.Add("OfficeID", employee.OfficeID);

            using (var connection = _context.CreateConnection())
            {
                await connection.ExecuteAsync(query, parameters);
            }
        }
        public async Task DeleteEmployee(int id)
        {
            var query = @"
                            delete from dbo.Employee
                            where EmployeeID = @id
                          ";
            using (var connection = _context.CreateConnection())
            {
                await connection.ExecuteAsync(query, new { id });
            }
        }

        
    }
}
