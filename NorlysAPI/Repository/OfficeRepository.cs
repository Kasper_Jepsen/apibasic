﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NorlysAPI.Contracts;
using NorlysAPI.Context;
using NorlysAPI.Models;
using Dapper;

namespace NorlysAPI.Repository
{
    public class OfficeRepository : IOfficeRepository
    {
        private readonly DapperContext _context;

        public OfficeRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Office>> GetOffices()
        {
            var query = "select OfficeID, OfficeName from dbo.Office";

            using (var connection = _context.CreateConnection())
            {
                var offices = await connection.QueryAsync<Office>(query);
                return offices.ToList();
            }
        }
        public async Task<Office> GetOffice(int id)
        {
            var query = "Select * from dbo.office where OfficeID = @id";

            using (var connection = _context.CreateConnection())
            {
                var office = await connection.QuerySingleOrDefaultAsync<Office>(query, new { id });

                return office;
            }
        }
    }
}
